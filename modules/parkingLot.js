var Car = require("./car.js");

/**
 * @description a base class for Parking lot
 */
class ParkingLot {
  constructor() {
    this.MAX_PARKING_SLOTS = 0; // maximum parking slots allowed
    this.parkingSlots = new Array(); // array for parking slots
  }

  /**
   *
   * @param {String} input
   * @description creates a parking lot with given maximum slot numbers.
   */
  createParkingLot(input) {
    this.MAX_PARKING_SLOTS = parseInt(input.split(" ")[1]);
    if (this.MAX_PARKING_SLOTS <= 0) {
      // minimum: 1 slot
      throw new Error("Minimum one slot is required to create parking slot");
    }
    for (var i = 0; i < this.MAX_PARKING_SLOTS; i++) {
      this.parkingSlots.push(null);
    }
    return this.MAX_PARKING_SLOTS;
  }

  /**
   *
   * @param {String} input
   * @description Assigns nearest slot number to cars to be parked.
   */
  parkCar(input) {
    let len = this.parkingSlots.length;
    if (this.MAX_PARKING_SLOTS > 0) {
      let car, carRegNumber, carDriverAge;
      if (this.findNearestAvailableSlot(this.parkingSlots) == true) {
        for (let i = 0; i < len; i++) {
          if (this.parkingSlots[i] == null) {
            carRegNumber = input.split(" ")[1];
            carDriverAge = input.split(" ")[3];
            if (carRegNumber && carDriverAge) {
              car = new Car(carRegNumber, carDriverAge);
              this.parkingSlots[i] = car;
              i = i + 1;
              let parkingLotObj = {
                regNumber: carRegNumber,
                age: carDriverAge,
                slot: i,
              };
              return parkingLotObj;
            } else {
              throw new Error(
                "Please provide registration number and age both"
              );
            }
          }
        }
      } else {
        throw new Error("Sorry, parking lot is full");
      }
    } else {
      throw new Error("Minimum one slot is required to create parking slot");
    }
  }

  /**
	 *
	 * @param {String} input 
	 * @description makes slot free for given slot number.
	 
	 */
  leaveCar(input) {
    if (this.MAX_PARKING_SLOTS > 0) {
      var index = parseInt(input.split(" ")[1] - 1);
      index = index > 0 ? index : 0;
      if (index >= this.MAX_PARKING_SLOTS) {
        throw new Error(`Slot number ${index + 1} is not found`);
      } else if (this.parkingSlots[index] === null) {
        throw new Error(`Slot number ${index + 1} is already vacant`);
      } else if (index > -1 && index <= this.parkingSlots.length) {
        const parkingLotObj = {
          regNumber: this.parkingSlots[index].NUMBER,
          age: this.parkingSlots[index].AGE,
          slot: index + 1,
        };
        this.parkingSlots[index] = null;
        index = index + 1;

        return parkingLotObj;
      }
    } else {
      throw new Error("Sorry, parking lot is empty");
    }
  }

  /**
   *
   * @param {String} input
   * @description returns slot number for given car number.
   */
  getSlotByCarNumber(input) {
    // TODO:  What parking lot is empty
    if (this.MAX_PARKING_SLOTS > 0) {
      var ele = "Not found";
      for (var i = 0; i < this.parkingSlots.length; i++) {
        if (
          this.parkingSlots[i] &&
          this.parkingSlots[i].NUMBER == input.split(" ")[1]
        ) {
          ele = i + 1;
        }
      }
      return ele;
    } else {
      return null;
    }
  }
  /**
	 * 
	 * @param {String}  
	 * @description returns slot number for a given age.
	
	 */
  getSlotByDriverAge(input) {
    if (this.MAX_PARKING_SLOTS > 0) {
      var ele = [];
      for (let i = 0; i < this.parkingSlots.length; i++) {
        if (
          this.parkingSlots[i] &&
          this.parkingSlots[i].AGE === input.split(" ")[1]
        ) {
          ele.push(i + 1);
        }
      }
      return ele;
    } else {
      return null;
    }
  }
  /**
	 * 
	 * @param {String} input 
	 * @description returns registration number for given age

	 */
  getRegistrationByDriverAge(input) {
    var ele = [];
    if (this.MAX_PARKING_SLOTS > 0) {
      for (let i = 0; i < this.parkingSlots.length; i++) {
        if (
          this.parkingSlots[i] &&
          this.parkingSlots[i].AGE === input.split(" ")[1]
        ) {
          ele.push(this.parkingSlots[i].NUMBER);
        }
      }
      return ele;
    } else {
      return null;
    }
  }

  /**
   * @description returns a comma separated string of free parking slots.
   * It returns `null` if parking lot is not created
   */

  findNearestAvailableSlot() {
    var ele = false;
    for (var i = 0; i < this.parkingSlots.length; i++) {
      if (this.parkingSlots[i] == null) {
        ele = true;
      }
    }
    return ele;
  }
}

module.exports = ParkingLot;
