#!/usr/bin/env node
const fs = require("fs"),
  readLine = require("readline");

var commandLineInputs = process.argv, // processing command line inputs
  interactiveMode = false;

/**
 * @description importing the parkingLot class
 */
var Parking = require("./modules/parkingLot.js"),
  parkingLot = new Parking();

// to avoid memory leaks errors, default max listeners = 10
require("events").EventEmitter.defaultMaxListeners = 0;

fs.readFile("./input.txt", "utf-8", function (err, data) {
  if (err) {
    console.log("Error in reading file");
  }
  var arr = data.split("\n");
  for (var i = 0; i < arr.length; i++) {
    console.log(arr[i]);
    processUserCommands(arr[i]);
  }

  // returning to console once all the inputs are processed
  process.exit(1);
});

function processUserCommands(input) {
  var userCommand = input.split(" ")[0],
    totalParkingSlots,
    parkingSlotNumber,
    parkingRegistrationNumber,
    parkingLotObject;
  switch (userCommand) {
    case "Create_parking_lot":
      try {
        totalParkingSlots = parkingLot.createParkingLot(input);
        console.log("Created  parking of " + totalParkingSlots + " slots");
      } catch (err) {
        console.log(err.message);
      }

      break;
    case "Park":
      try {
        parkingLotObject = parkingLot.parkCar(input);
        console.log(
          `Car with vehicle registration number "${parkingLotObject.regNumber}" has been parked at slot number ${parkingLotObject.slot}`
        );
      } catch (err) {
        console.log(err.message);
      }
      break;
    case "Leave":
      try {
        parkingLotObject = parkingLot.leaveCar(input);
        console.log(
          `Slot number ${parkingLotObject.slot} vacated, the car with vehicle registration number "${parkingLotObject.regNumber}" left the space, the driver of the car was of age ${parkingLotObject.age}`
        );
      } catch (err) {
        console.log(err.message);
      }
      break;
    case "Slot_number_for_car_with_number":
      parkingSlotNumber = parkingLot.getSlotByCarNumber(input);
      if (parkingSlotNumber) {
        console.log(parkingSlotNumber);
      } else {
        console.log("Car with given registration number is not found");
      }
      break;
    case "Slot_numbers_for_driver_of_age":
      parkingSlotNumber = parkingLot.getSlotByDriverAge(input);
      if (parkingSlotNumber.length) {
        console.log(parkingSlotNumber.join(", "));
      } else {
        console.log("Car with given driver age is not found");
      }
      break;
    case "Vehicle_registration_number_for_driver_of_age":
      parkingRegistrationNumber = parkingLot.getRegistrationByDriverAge(input);
      if (parkingRegistrationNumber.length) {
        console.log(parkingRegistrationNumber.join(", "));
      } else {
        console.log("Car with given driver age is not found");
      }
      break;
    case "exit":
      process.exit(0);
      break;
    default:
      console.log(input, "is an invalid command");
      break;
  }
}
