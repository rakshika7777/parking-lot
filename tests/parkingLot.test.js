var Parking = require('../modules/parkingLot.js')


  describe('test createParkingLot()', () => {
      parkingLot = new Parking();

    test("defines createParkingLot()", () => {
        expect(typeof parkingLot.createParkingLot).toBe("function");
      
    });
      test('create parkingLot with 2 slots', () => {
        expect(parkingLot.createParkingLot('Create_parking_lot 2')).toBe(2)
    })
  })

 describe('test parkCar()', () => {

   test("defines parkCar()", () => {
     expect(typeof parkingLot.parkCar).toBe("function");
   });
     test('park car in parking lot', () => {
         expect(parkingLot.parkCar('Park KA-02-HH-1234 driver_age 18')).toEqual(expect.objectContaining({"age": "18", "regNumber": "KA-02-HH-1234", "slot": 1}));
     })
   test('throw error if car number or driver age is missing', () => {
        
     expect(() => {
       parkingLot.parkCar('Park driver_age 18')
     }).toThrow(Error);
     expect(() => {
       parkingLot.parkCar('Park driver_age 18')
   }).toThrow('Please provide registration number and age both');

     })
   test('throw error if all parking slots are full', () => {
     expect(parkingLot.parkCar('Park MA-02-HH-1235 driver_age 23')).toEqual(expect.objectContaining({ "age": "23", "regNumber": "MA-02-HH-1235", "slot": 2 }));

       expect(() => {
         parkingLot.parkCar('Park LA-02-HH-1235 driver_age 33')
       }).toThrow(Error);
       expect(() => {
         parkingLot.parkCar('Park LA-02-HH-1235 driver_age 33')
       }).toThrow('Sorry, parking lot is full');
     })
    
 })

  describe('test leaveCar()', () => {

    test("defines leaveCar()", () => {
        expect(typeof parkingLot.leaveCar).toBe("function");
    });
    test('Should make the  specified car slot empty', () => {
      expect(parkingLot.leaveCar('Leave 2')).toEqual(expect.objectContaining({ "age": "23", "regNumber": "MA-02-HH-1235", "slot": 2 }));
      expect(parkingLot.leaveCar('Leave 1')).toEqual(expect.objectContaining({"age": "18", "regNumber": "KA-02-HH-1234", "slot": 1}));

    })
    
      test("Throw error if slot is already free", () => {
        expect(() => {
          parkingLot.leaveCar('Leave 2')
        }).toThrow(Error);
        expect(() => {
          parkingLot.leaveCar('Leave 2')
        }).toThrow('Slot number 2 is already vacant');

      })
   
      test("Throw error if slot does not exist", () => {
        expect(() => {
          parkingLot.leaveCar('Leave 6')
        }).toThrow(Error);
        expect(() => {
          parkingLot.leaveCar('Leave 6')
        }).toThrow('Slot number 6 is not found');
      })
    
  })

  describe('test getSlotByCarNumber()', () => {
   

    test("defines getSlotByCarNumber()", () => {
      expect(typeof parkingLot.getSlotByCarNumber).toBe("function");
    });
    test("Return error if slot is empty", () => {
      expect(parkingLot.getSlotByCarNumber("Slot_number_for_car_with_number KA-01-HH-1234")).toBe('Not found');
    })
  
    test("if slot is not empty and car number is valid", () =>
    {
          parkingLot.parkCar("Park KA-05-HH-1234 driver_age 18");

      expect(parkingLot.getSlotByCarNumber("Slot_number_for_car_with_number KA-05-HH-1234")).toBe(1);
   })
  })

 describe('test getSlotByDriverAge()', () => {

   test("defines getSlotByDriverAge()", () => {
     expect(typeof parkingLot.getSlotByDriverAge).toBe("function");

   });
  test("if slot is not empty and car number is valid", () =>
   {
         parkingLot.parkCar("Park KA-04-HH-1234 driver_age 22");

    expect(parkingLot.getSlotByDriverAge("Slot_numbers_for_driver_of_age 22").length).toBe(1);
     expect(parkingLot.getSlotByDriverAge("Slot_numbers_for_driver_of_age 22")).toEqual(expect.arrayContaining([2]))
  })

 })

describe('test getRegistrationNumberBy()', () => {
    test("defines getRegistrationByDriverAge()", () => {
     expect(typeof parkingLot.getRegistrationByDriverAge).toBe("function");

    });
  test("Return not found if driver age is incorrect", () => {
      expect(parkingLot.getRegistrationByDriverAge("Vehicle_registration_number_for_driver_of_age 48")).toEqual([])

  })
  test("Return correct vehicle number if driver age is correct", () => {
    expect(parkingLot.getRegistrationByDriverAge("Vehicle_registration_number_for_driver_of_age 18")).toEqual(expect.arrayContaining(['KA-05-HH-1234']))

  })

 })